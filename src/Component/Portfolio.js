import React, { Component } from 'react'
import myimage from '../abc.jpg'

export default class Portfolio extends Component {
    render() {
        return (
            <div class="header-wraper">
                <div class="main-info">
                    <h1>My Portfolio</h1>
                </div>
                <img src={myimage} />
                <div class="img" id="image">
                <div class="about-me">
                    <div class="row">
                        <div class="col-lg-4" data-aos="fade-right">
                        </div>
                        <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
                            <h2>About me</h2>
                            
                            <p class="fst-italic">
                                Hey thanks for checking my page. I'm a passed out student of Itahari International College.<br />
                                Recently completed Bsc Computing. I have created this portfolio by the help of Bootstrap in React.
                            </p>
                            <div class="row">
                                <div class="col-lg-6">
                                    <ul>
                                        <li><i class="bi bi-chevron-right"></i> <strong>Birthday:</strong> <span>14 Nov 1998</span></li>
                                        <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>9804307310</span></li>
                                        <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>Morang, Nepal</span></li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <ul>
                                        <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span>22</span></li>
                                        <li><i class="bi bi-chevron-right"></i> <strong>Degree:</strong> <span>Bachloer</span></li>
                                        <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span>Yubinpokhrel80@gmail.com</span></li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <ul>
                                        <li><i class="bi bi-chevron-right"></i> <strong>Experience:</strong> <span>3 Month Internship at GrayCode in C#</span></li>&nbsp;

                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </div>
                <div class="footer">
                    <p> <a href="https://www.instagram.com/yubinpokhrel/" class="btn btn-success btn-lg active" role="button" aria-pressed="true">Follow on Instagram</a>&nbsp;
                        <a href="https://gitlab.com/YubinPokhrel/formapp" class="btn btn-success btn-lg active" role="button" aria-pressed="true">Gitlab Project</a>&nbsp;
                        <a href="https://www.linkedin.com/in/yubin-pokhrel-65226518a/" class="btn btn-success btn-lg active" role="button" aria-pressed="true">Linked-In</a></p>
                </div>
            </div>





        )
    }
}
